package com.example.inpost.client;


import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BoxRepository extends JpaRepository<Box, Long> {
	Optional<Box> findBoxByBoxNumber(String boxNumber);
}
