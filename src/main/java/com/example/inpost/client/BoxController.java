package com.example.inpost.client;

import java.util.List;
import java.util.Optional;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/inpost")
@Slf4j
public class BoxController {

	private final BoxRepository boxRepository;

	//POST - dodaj nowa skrzynke
	@PostMapping
	public Box addNewBox(@RequestBody Box box) {
		return boxRepository.save(box);
	}

	//GET - wypisz wszystkie skrzynki
	@GetMapping
	public List<Box> findAll() {
		return boxRepository.findAll();
	}

	@PostMapping("changestate")
//	@Transactional
	public ResponseEntity<Box> changeState(@RequestBody Box box) {
		//find by name
		log.warn("trying to open box:{}", box);
		Optional<Box> boxByBoxNumber = boxRepository
				.findBoxByBoxNumber(box.getBoxNumber());

		if (boxByBoxNumber.isPresent()) {
			Box boxFromDb = boxByBoxNumber.get();
			log.warn("box state updated from {} to {}",boxFromDb.isState(), box.isState());
			boxFromDb.setState(box.isState());
			boxRepository.save(boxFromDb);
			return ResponseEntity.status(HttpStatus.ACCEPTED)
					   .body(boxFromDb);
		}
		log.warn("box not found");
		return ResponseEntity.status(HttpStatus.NOT_FOUND)
			.body(box);
	}
}
